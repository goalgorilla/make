; core.make
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Include core
; ------------
includes[core] = "includes/core.make"

; Recommended modules
projects[email][type] = "module"
projects[email][subdir] = "contrib"
projects[link][type] = "module"
projects[link][subdir] = "contrib"
projects[boxes][type] = "module"
projects[boxes][subdir] = "contrib"
projects[jquery_update][type] = "module"
projects[jquery_update][subdir] = "contrib"
projects[jquery_ui][type] = "module"
projects[jquery_ui][subdir] = "contrib"

; Administration modules
includes[administration] = "includes/administration.make"

; Developer modules
includes[devel] = "includes/devel.make"
