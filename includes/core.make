; core.make
; ----------------
;
; This is a working makefile - try it! Any line starting with a `;` is a comment.
  
; Core version
; ------------
; Each makefile should begin by declaring the core version of Drupal that all
; projects should be compatible with.
  
core = 7.x
  
; API version
; ------------
; Every makefile needs to declare its Drush Make API version. This version of
; drush make uses API version `2`.
  
api = 2
  
; Core project
; ------------
; In order for your makefile to generate a full Drupal site, you must include
; a core project. This is usually Drupal core, but you can also specify
; alternative core projects like Pressflow. Note that makefiles included with
; install profiles *should not* include a core project.
  
; Drupal 7.x. Requires the `core` property to be set to 7.x.
projects[drupal][version] = 7
  
; Modules
; --------

; Required modules
projects[ctools][type] = "module"
projects[ctools][subdir] = "contrib"
projects[context][type] = "module"
projects[context][subdir] = "contrib"
projects[features][type] = "module"
projects[features][subdir] = "contrib"
projects[strongarm][type] = "module"
projects[strongarm][subdir] = "contrib"
projects[token][type] = "module"
projects[token][subdir] = "contrib"
projects[views][type] = "module"
projects[views][subdir] = "contrib"
projects[date][type] = "module"
projects[date][subdir] = "contrib"
projects[libraries][type] = "module"
projects[libraries][subdir] = "contrib"


; Themes
; --------
projects[zen][type] = "module"
projects[goalgorilla][download][type] = "theme"
projects[goalgorilla][download][type] = "git"
projects[goalgorilla][download][url] = "git@bitbucket.org:goalgorilla/goalgorilla-base-theme.git"
  
  
; Libraries
; ---------
libraries[jquery][download][type] = "file"
libraries[jquery][download][url] = "https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"
libraries[jqueryui][download][type] = "file"
libraries[jqueryui][download][url] = "https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"
